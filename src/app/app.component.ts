import { Component, OnInit } from '@angular/core';
import { RestService } from './rest.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'REST dla Kursantow';
  osoby: Kursant[] = new Array<Kursant>();

  joke = 'Trwa pobieranie kawalu ...';

  constructor(private restService: RestService) {}

  ngOnInit(): void {
    console.log('poczatek ngOnInit()');
    // this.osoby.push({id: 1, firstName: 'Dariusz', lastName: 'Mach'});
    // this.osoby.push({id: 2, firstName: 'Kasia', lastName: 'Faras'});
    // this.osoby.push({id: 3, firstName: 'Mirek', lastName: 'Malik'});
    this.osoby.push({id: 4, firstName: 'Robert', lastName: 'Kareta'});
    // this.osoby.push({id: 5, firstName: 'Pawel', lastName: 'Bajda'});

    this.restService.getChuckNorrisJoke().subscribe(
      data => {
        console.log('pobralo rest z Chuck Norris');
        console.log(data);
        // specjalnie dodalem 2s opoznienia
        setTimeout(() => {
          this.joke = data.value;
          console.log('przypisano dane z Chuck Norris');
        }, 2000);
      }
    );

    this.restService.getKursanci().subscribe(
      kursanciZResta => {
        console.log('pobralo rest z Kursanci');
        // specjalnie dodalem 4s opoznienia
        setTimeout(() => {
          this.osoby = kursanciZResta;
          console.log('przypisano dane z Kursanci');
        }, 4000);
      }
    );

    console.log('koniec ngOnInit()');
  }

}

export interface Kursant {
  id: number;
  firstName: string;
  lastName: string;
}
