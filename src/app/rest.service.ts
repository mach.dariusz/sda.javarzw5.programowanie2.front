import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Kursant } from './app.component';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }

  getChuckNorrisJoke(): Observable<any> {
    return this.http.get('https://api.chucknorris.io/jokes/random');
  }

  getKursanci(): Observable<Kursant[]> {
    return this.http.get<Kursant[]>('http://localhost:8080/api/kursanci');
  }
}
